import { Routes, RouterModule } from "@angular/router";
import { CanActivate }      from "@angular/router";

import { AuthGuard }        from "../app/services/loggedin-service";

import { Admin }            from "../app/admin/admin";
import { About }            from "../app/about/about";
import { CourierReg }       from "../app/couriers/registration/courier-reg";
import { CreateDelivery }   from "../app/customers/deliveries/delivery-creation";
import { CustomerDeliveryHome } from "../app/customers/deliveries/delivery-home";
import { Home }             from "../app/home/home";
import { Footer }           from "../app/footer/footer";
import { Profile }          from "../app/shared/profile/profile";
import { AddProfileInfo }   from "../app/shared/profile/add-profile-info";
import { EditProfile }      from "../app/shared/profile/edit-profile";
import { EditVehicle }      from "../app/couriers/vehicle/edit-vehicle";
import { UniversalLogin }   from "../app/shared/login/universal-login";
import { SupportComponent } from "../app/support/support";


export const routes: Routes = [
    {
      path: "",
      component: Home
    },
    {
      path: "admin",
      component: Admin
    },
    {
      path: "home",
      component: Home
    },
    {
      path: "about",
      component: About
    },
    {
      path: "login",
      component: UniversalLogin
    },
    {
      path: 'profile',
      component: Profile,
      canActivate: [AuthGuard]
    },
    {
      path:"add_profile_info",
      component: AddProfileInfo,
      canActivate: [AuthGuard]
    },
    {
      path: "edit_profile",
      component: EditProfile,
      canActivate: [AuthGuard]
    },
    {
      path: "edit_vehicle",
      component: EditVehicle,
      canActivate: [AuthGuard]
    },
    {
      path: "courier_reg",
      component: CourierReg
    },
    {
      path: "create_delivery",
      component: CreateDelivery,
      canActivate: [AuthGuard]
    },
    {
      path: 'customer_delivery_home',
      component: CustomerDeliveryHome,
      canActivate: [AuthGuard]
    },
    {
      path: "support",
      component: SupportComponent
    }
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(routes);
