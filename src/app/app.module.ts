import { NgModule, TemplateRef, provide, OnInit } from '@angular/core';
import { BrowserModule  }   from '@angular/platform-browser';
import { FormsModule }      from '@angular/forms';
import { RouterModule }     from '@angular/router';

import { AppComponent }     from './app.component';
import { Admin }            from "../app/admin/admin";
import { Home }             from "./home/home";
import { About }            from './about/about';
import { CustomerRegForm }  from "../app/customers/registration/customer-reg-form";
import { CreateDelivery }   from "../app/customers/deliveries/delivery-creation";
import { CustomerDeliveryHome } from "../app/customers/deliveries/delivery-home";
import { UniversalLogin }   from "../app/shared/login/universal-login";
import { Profile }          from "../app/shared/profile/profile";
import { AddProfileInfo }   from "../app/shared/profile/add-profile-info";
import { EditProfile }      from "../app/shared/profile/edit-profile";
import { EditVehicle }      from "../app/couriers/vehicle/edit-vehicle";
import { CourierReg }       from "../app/couriers/registration/courier-reg";
import { SupportComponent } from "../app/support/support";
import { Footer }           from "../app/footer/footer";
import { HTTP_PROVIDERS, BaseRequestOptions, Headers, RequestOptions } from "@angular/http";

import { APIService }       from './services/api-service';
import { DataService }      from './services/data-service';
import { LocalStorageService } from "./services/localstorage-service";
import { AuthGuard }        from './services/loggedin-service';
import { routing }          from './app.routes';
import { HttpModule, JsonpModule } from '@angular/http';

class AppBaseRequestOptions extends BaseRequestOptions {
  private authKey = localStorage.getItem("token");
  headers: Headers = new Headers({
    "Content-Type":"application/json; charset=utf-8",
    "Authorization": "Token " + this.authKey,
    "Accept":"application/json"
  })
}

@NgModule({
  declarations: [
    AppComponent,
    Admin,
    Home,
    About,
    CreateDelivery,
    CustomerDeliveryHome,
    UniversalLogin,
    CustomerRegForm,
    Profile,
    AddProfileInfo,
    EditProfile,
    EditVehicle,
    CourierReg,
    SupportComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule,
    JsonpModule
  ],
  providers: [
    DataService,
    APIService,
    LocalStorageService,
    AuthGuard,
    TemplateRef,
    OnInit,
    HTTP_PROVIDERS,
    provide(
      RequestOptions, {useClass: AppBaseRequestOptions})
  ],
    bootstrap: [ AppComponent ],
})

export class AppModule {}
