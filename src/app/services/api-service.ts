import { Http, Response, Request } from '@angular/http';
import { Injectable } from '@angular/core';
import { DataService } from './data-service';

var user_id = localStorage.getItem("user_id");
var courier_ep      = 'http://127.0.0.1:8000/api/v1/couriers/';
var customer_ep     = 'http://127.0.0.1:8000/api/v1/customers/';
var customer_info   = 'http://127.0.0.1:8000/api/v1/customer_info/'+user_id+'/'
var edit_courier    = 'http://127.0.0.1:8000/api/v1/couriers/'+user_id+'/';
var edit_customer   = 'http://127.0.0.1:8000/api/v1/customers/'+user_id+'/';
var cust_deliveries = 'http://127.0.0.1:8000/api/v1/deliveries/';
var login           = 'http://127.0.0.1:8000/api/v1/auth/login/';
var register        = 'http://127.0.0.1:8000/api/v1/auth/register/';
var support         = 'http://127.0.0.1:8000/api/v1/support/';
@Injectable()
export class APIService {
// Change all endpoints in production
  private _couriersAPI:     string = courier_ep;
  private _customersAPI:    string = customer_ep;
  private _customerInfoAPI: string = customer_info;
  private _editCourierAPI:  string = edit_courier;
  private _editCustomerAPI: string = edit_customer;
  private _custDeliveryAPI: string = cust_deliveries;
  private _supportAPI:      string = support;
  private _loginAPI:        string = login;
  private _registerAPI:     string = register;

  constructor(public dataService: DataService) { }

  // Admin
  getAllCustomers() {
    return this.dataService.get(this._customersAPI)
  }
  getAllCouriers() {
    return this.dataService.get(this._couriersAPI)
  }

  // Registration
  register(user) {
    return this.dataService.post(this._registerAPI, user);
  }
  // Login
  login(user) {
    return this.dataService.post(this._loginAPI, user);
  }
  // Support
  support(support){
    return this.dataService.post(this._supportAPI, support)
  }
  // Customer Information
  addCustomerInfo(customer) {
    return this.dataService.post(this._customersAPI, customer);
  }
  editCustomerInfo(customer) {
    return this.dataService.put(this._editCustomerAPI, customer);
  }
  getCustomerInfo() {
    return this.dataService.get(this._customerInfoAPI);
  }
  //Courier Stuff
  addCourierInfo(courier) {
    return this.dataService.post(this._couriersAPI, courier);
  }
  editCourierInfo(courier) {
    return this.dataService.put(this._editCourierAPI, courier);
  }
  getCourierInfo() {
    return this.dataService.get(this._couriersAPI);
  }
  // Customer Deliveries
  createDelivery(delivery) {
    return this.dataService.post(this._custDeliveryAPI, delivery);
  }
  getCustomerDeliveries() {
    return this.dataService.get(this._custDeliveryAPI);
  }
}
