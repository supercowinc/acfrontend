import {Injectable} from "@angular/core";

@Injectable()
export class LocalStorageService {

    constructor(){ }

    public setKey(key:string){
        localStorage.setItem("token", JSON.stringify(key))
    }

    public getKey(){
        return localStorage.getItem("token")
    }
    public getUserType() {
        return localStorage.getItem("user_type");
    }
    public getUserId() {
        return localStorage.getItem("user_id");
    }
}
