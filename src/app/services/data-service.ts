import { Http, Response, Request, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DataService{

    constructor(public http: Http) { }

    get(uri: string, mapJson: boolean = true) {
        var http: any = this.http;

        if (mapJson)
            return http.get(uri)
                .map((res: Response) => res.json());
        else
            return http.get(uri);
    }

    post(uri: string, data?: any, mapJson: boolean = true) {
        var http: any = this.http;

        if (mapJson)
            return http.post(uri, data)
                .map(response => <any>(<Response>response).json());
        else
            return http.post(uri, data);
    }

    put(uri: string, data?:any) {
      console.log(data);
      let body = JSON.stringify(data)
      return this.http.put(uri, body).map((res: Response) => res.json());
    }

    delete(uri: string) {
        var http: any = this.http;

        return http.delete(uri)
            .map(response => <any>(<Response>response).json())
    }
}
