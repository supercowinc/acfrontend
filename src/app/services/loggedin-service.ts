import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { LocalStorageService } from "./localstorage-service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _router: Router) { }

  isLoggedIn() {
    var token = localStorage.getItem("token");
    if (token != null) {
      return true
    }
    else {
      return false
    }
  }

  canActivate() {
    if (this.isLoggedIn() === false) {
      this._router.navigate(['login']);
    }
      return this.isLoggedIn()
  }
}
