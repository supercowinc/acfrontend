import { Component } from "@angular/core";
import { FORM_DIRECTIVES } from "@angular/forms";

import { Support } from "../../app/models/support/support";
import { APIService } from "../services/api-service";

@Component({
  moduleId: module.id,
  selector: "support",
  templateUrl: "./support.html",
  directives: [FORM_DIRECTIVES]
})

export class SupportComponent {

  constructor(private APIService: APIService) { }

  support: Support = new Support()

  onSubmit() {
    console.log(this.support)
    this.APIService.support(this.support).subscribe();
  }
}
