export class Customer {
  public user: string;
  public business_name: string;
  public phone_number: string;
  public address: string;
  public address_2: string;
  public city: string;
  public state: string;
  public zip_code: string;

  constructor(obj?: any) {
    if (obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) this[prop] = obj[prop];
      }
    }
  }
}
