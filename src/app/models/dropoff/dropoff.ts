import { Location } from "../../models/location/location";

export class Dropoff {
    public location: Location;
    public SignatureRequired: boolean;
    public SpecialInstructions: string;
}
