import { Vehicle } from "../../models/vehicle/vehicle";
import { Location } from "../../models/location/location";
import { Phone } from "../../models/phone/phone";

export class Courier {
  public user: string;
  public first_name: string;
  public last_name: string;
  public phone_number: string;
  public courier_status: string;
  public license_number: string;
  public insurance_policy_number: string;
  public insurance_expiration_date: Date;

  constructor(obj?: any) {
    if (obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) this[prop] = obj[prop];
      }
    }
  }
}
