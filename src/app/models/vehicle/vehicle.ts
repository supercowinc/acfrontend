export class Vehicle {
    public LicensePlate: string;
    public make: string;
    public model: string;
    public pictureUrl: string;
}
