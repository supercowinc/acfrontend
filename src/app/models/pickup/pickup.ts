import { Location } from "../../models/location/location";

export class Pickup {
    public location: Location;
    public SignatureRequired: boolean;
    public SpecialInstructions: string;
}
