import { Courier } from "../../models/courier/courier";

export class Delivery {
    public CreatedAt: Date;
    public customer: string;
    public status: string;
    public pickup_business_name: string;
    public pickup_phone_number: string;
    public pickup_address: string;
    public pickup_address2: string;
    public pickup_city: string;
    public pickup_state: string;
    public pickup_zipCode: number;
    public pickup_special_instructions: string;
    public dropoff_business_name: string;
    public dropoff_phone_number: string;
    public dropoff_address: string;
    public dropoff_address2: string;
    public dropoff_city: string;
    public dropoff_state: string;
    public dropoff_zipCode: number;
    public dropoff_signature_required: boolean;
    public dropoff_special_instructions: string;
    public fee: string;
    public TrackingURL: string;
}
