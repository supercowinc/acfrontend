export class Support {
    business_name: string;
    first_name: string;
    last_name: string;
    email: string;
    phone_number: string;
    subject: string;
    message: string;
}
