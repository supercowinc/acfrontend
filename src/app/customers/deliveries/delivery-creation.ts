import { Component }  from "@angular/core";
import { Router }     from "@angular/router";

import { Delivery }   from "../../models/delivery/delivery";

import { APIService } from "../../services/api-service";
import { LocalStorageService } from "../../services/localstorage-service";

@Component({
  moduleId: module.id,
  selector: 'create-delivery',
  templateUrl: './delivery-creation.html'
})

export class CreateDelivery {

  constructor(
    private APIService: APIService,
    private _route: Router,
    private localStorage: LocalStorageService
  ) { }

  private delivery: Delivery = new Delivery()

  onSubmit() {
    var user_id = this.localStorage.getUserId()
    this.delivery.customer = user_id
    this.delivery.status = 'Scheduled'
    this.APIService.createDelivery(this.delivery).subscribe(
      res => {
        this._route.navigate(['customer_delivery_home'])
      },
      error => console.log(error)
    );
  }
}
