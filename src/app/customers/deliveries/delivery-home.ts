import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Delivery } from "../../models/delivery/delivery";
import { Http } from "@angular/http";

import { APIService } from "../../services/api-service";

@Component({
  moduleId: module.id,
  selector: 'customer-delivery-home',
  templateUrl: './delivery-home.html',
  styles: ['ul {list-style-type: none;}']
})

export class CustomerDeliveryHome implements OnInit {
  public delivery = new Array<Delivery>()

  constructor(private APIService: APIService) { }

  ngOnInit() {
    this.APIService.getCustomerDeliveries().subscribe(
      res => this.delivery = res,
      err=> console.log(err)
    )
  }
}
