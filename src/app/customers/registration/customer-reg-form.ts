import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { APIService } from "../../services/api-service";

import { User } from "../../models/user/user";

@Component({
  moduleId: module.id,
  selector: "customer-reg-form",
  templateUrl: "./customer-reg-form.html",
})

export class CustomerRegForm {

  constructor(
    private APIService: APIService,
    private _router: Router
  ) { }

  private user: User = new User()

  onSubmit() {
    this.user.user_type = 'customer';
    this.APIService.register(this.user).subscribe(
      res => {
        this._router.navigate(['login']);
      },
      error => alert("That user already exists!")
    );
  }
}
