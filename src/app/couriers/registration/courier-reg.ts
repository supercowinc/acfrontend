import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { APIService } from "../../services/api-service";

import { User } from "../../models/user/user";

@Component({
  moduleId: module.id,
  selector: "courierReg",
  templateUrl: "./courier-reg.html",
})

export class CourierReg {

  constructor(
    private APIService: APIService,
    private router: Router
  ) { }

  private user: User = new User()

  onSubmit() {
    this.user.user_type = 'courier'
    this.APIService.register(this.user).subscribe(
      res => this.router.navigate(['login']),
      error => console.log(error)
    );
  }
}
