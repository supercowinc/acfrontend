import { Component, OnInit }    from "@angular/core";
import { Router }       from "@angular/router";
import { Http, Response }         from "@angular/http";

import { APIService }   from "../../services/api-service";
import { AuthGuard }    from "../../services/loggedin-service";
import { LocalStorageService } from "../../services/localstorage-service";

import { Customer }     from "../../models/customer/customer";
import { Courier }      from "../../models/courier/courier";

@Component({
  moduleId: module.id,
  selector: "editProfile",
  templateUrl: "./edit-profile.html"
})

export class EditProfile implements OnInit {

  private customer: Customer = new Customer()
  private courier: Courier = new Courier()

  constructor(
    private localStorage: LocalStorageService,
    private APIService: APIService,
    private _router: Router,
    private http: Http
  ) { }

  ngOnInit() {
    var user_type = localStorage.getItem("user_type");
    if (user_type === 'customer') {
      this.APIService.getCustomerInfo().subscribe(
        res => {
          console.log(res);
          if (res.length === 0) {
            this._router.navigate(['add_profile_info'])
          }
          else {
            this.customer = new Customer(res[0]);
          }
        },
        err=> console.error(err)
      )
    }
    else {
      this.APIService.getCourierInfo().subscribe(
        res => {
          if (res.length===0) {
            this._router.navigate(['add_profile_info'])
          }
          else {
            this.courier = new Courier(res[0]);
          }
        },
        err => console.error(err)
      )
    }
  }

  UserType() {
    return this.localStorage.getUserType()
  }

  onCustomerSubmit() {
    var user_id = this.localStorage.getUserId()
    this.customer.user = user_id;
    this.APIService.editCustomerInfo(this.customer).subscribe(
      res => {
        this._router.navigate(['profile']);
      },
      err => {
        console.error(err)
      }
    );
  };
  onCourierSubmit() {
    var user_id = this.localStorage.getUserId()
    this.courier.user = user_id;
    console.log(this.courier)
    this.APIService.editCourierInfo(this.courier).subscribe(
      res => {
        this._router.navigate(['profile']);
      },
      err => {
        console.error(err)
      }
    )
  };
};
