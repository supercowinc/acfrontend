import { Component }    from "@angular/core";
import { Router }       from "@angular/router";

import { APIService }   from "../../services/api-service";
import { AuthGuard }    from "../../services/loggedin-service";
import { LocalStorageService } from "../../services/localstorage-service";

import { Customer }     from "../../models/customer/customer";
import { Courier }      from "../../models/courier/courier";

@Component({
  moduleId: module.id,
  selector:'add-profile-info',
  templateUrl: './edit-profile.html'
})

export class AddProfileInfo {

  private customer: Customer = new Customer()
  private courier: Courier = new Courier()

  constructor(
    private localStorage: LocalStorageService,
    private APIService: APIService,
    private _router: Router
  ) { }

  UserType() {
    return this.localStorage.getUserType()
  }

  onCustomerSubmit() {
    var user_id = this.localStorage.getUserId()
    this.customer.user = user_id;
    console.log(this.customer)
    this.APIService.addCustomerInfo(this.customer).subscribe(
      res => {
        this._router.navigate(['profile']);
      },
      err => {
        console.log(err)
      }
    );
  }
  onCourierSubmit() {
    var user_id = this.localStorage.getUserId()
    this.courier.user = user_id;
    console.log(this.courier)
    this.APIService.addCourierInfo(this.courier).subscribe(
      res => {
        console.log(res)
        this._router.navigate(['profile']);
      },
      err => {
        console.log(err)
      }
    )
  };
}
