import { Component, OnInit } from "@angular/core";
import { Router }     from "@angular/router";

import { APIService } from "../../services/api-service";
import { LocalStorageService } from "../../services/localstorage-service";

import { Customer }   from "../../models/customer/customer";
import { Courier }    from "../../models/courier/courier";

@Component({
  moduleId: module.id,
  selector: 'profile',
  templateUrl: './profile.html'
})

export class Profile implements OnInit {
  public customer = new Array<Customer>()
  public courier = new Array<Courier>()

  constructor(
    private localStorage: LocalStorageService,
    private router: Router,
    private APIService: APIService
  ) { }

  ngOnInit() {
    var user_type = localStorage.getItem("user_type");
    if (user_type === 'customer') {
      this.APIService.getCustomerInfo().subscribe(
        res => {
          this.customer = res
        },
        err=> {
          console.log(err)
          this.router.navigate(['profile'])
        }
      )
    }
    else {
      this.APIService.getCourierInfo().subscribe(
        res => {
          console.log(res)
          this.courier = res
        },
        err => console.log(err)
      )
    }
  }

  UserType() {
    return this.localStorage.getUserType()
  }
}
