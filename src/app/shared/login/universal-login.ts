import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { APIService } from "../../services/api-service";
import { LocalStorageService } from "../../services/localstorage-service";
import { User } from "../../models/user/user";

@Component({
  moduleId: module.id,
  selector: "login-form",
  templateUrl: "./universal-login.html",
})


export class UniversalLogin {
  token: string;

  constructor(
    private APIService: APIService,
    private _localStorage: LocalStorageService,
    private _router: Router) { }

  public user = new User();
  public errormsg = '';

  Login() {
    this.APIService.login(this.user).subscribe(
      res => {
        console.log(res)
        localStorage.setItem('token', res.token);
        localStorage.setItem('user_type', res.user_type);
        localStorage.setItem('user_id', res.user_id);
        if(res.user_type == 'customer') {
          this._router.navigate(['profile']);
        }
        else {
          this._router.navigate(['profile']);
        }
    },
      error => {
        console.log(error);
      }
    )};

  }
