import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { Footer } from "../app/footer/footer";

import { AuthGuard }     from "./services/loggedin-service";
import { LocalStorageService } from "./services/localstorage-service";

const template = './app.component.html'
const style = 'app.component.css'

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: template,
  styleUrls: [ style ]
})

export class AppComponent {

  constructor(
    private _router: Router,
    private _authGuard: AuthGuard
  ) { }

  Logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user_type');
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_email');
    this._router.navigate(['login'])
  }

  isLoggedIn() {
    return this._authGuard.isLoggedIn()
  }

}
