import { Component, OnInit } from "@angular/core";

import { APIService } from "../services/api-service";

import { Customer } from "../models/customer/customer";
import { Courier } from "../models/courier/courier";


@Component({
  moduleId: module.id,
  selector: 'admin',
  templateUrl: './admin.html'
})

export class Admin implements OnInit {
  public customer = new Array<Customer>()
  public courier = new Array<Courier>()

  constructor (
    private APIService: APIService
  ) { }

  ngOnInit() {
    this.APIService.getAllCustomers().subscribe(
      res => {
        this.customer = res
      },
      err => {
        console.log(err)
      }
    ),
    this.APIService.getAllCouriers().subscribe(
      res => {
        console.log(res)
        this.courier = res
      },
      err => {
        console.log(err)
      }
    )
  }
}
