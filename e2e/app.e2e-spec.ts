import { Acfrontend1Page } from './app.po';

describe('acfrontend1 App', function() {
  let page: Acfrontend1Page;

  beforeEach(() => {
    page = new Acfrontend1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
