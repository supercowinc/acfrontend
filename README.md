# README #

Please git clone this and enter the directory and type "ng serve" to run this application.

The endpoints are saved on the backendapi project, so those will not be active, this is purely the front end.

### What is this repository for? ###

* The front end user facing application
* Version 1
### Customer facing features that need to be added ###

* The forms on the Create Delivery page need to have integration with google maps autocomplete API and after form submission need to be redirected to a page with a list of those deliveries that have already been entered.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact